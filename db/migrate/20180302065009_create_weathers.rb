class CreateWeathers < ActiveRecord::Migration
  def change
    create_table :weathers do |t|
      t.integer :external_id
      t.timestamp :date_recorded
      t.string :temperature #, precision: 4, scale: 1, array: true
      t.integer :location_id
    end
  end
end
