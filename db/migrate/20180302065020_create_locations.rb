class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.decimal :lat, precision: 10, scale: 4
      t.decimal :lon, precision: 10, scale: 4
      t.string :state
      t.string :city
    end
  end
end
