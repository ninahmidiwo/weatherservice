Rails.application.routes.draw do
  defaults format: :json do
    post 'weather', to: 'weathers#add'
    get 'weather', to: 'weathers#list_all'
    get 'weather?date=:date', to: 'weathers#list_by_date'
    get 'weather?lat=:latitude&lon=:longitude', to: 'weathers#list_by_location'
    delete 'erase', to: 'weathers#erase_all'
    delete 'erase?start=:startDate&end=:endDate&lat=:latitude&lon=:longitude', to: 'weathers#erase'
  end
end
