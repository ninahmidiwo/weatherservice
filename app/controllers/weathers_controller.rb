class WeathersController < ApplicationController
  def add
    location_params = weather_params.delete('location').to_h
    weather_hash = weather_params.permit(:id, :date, :temperature => []).to_h
    weather = Weather.find_by(external_id: params[:id])
    message = 'Weather exists'
    render json: { result: message }.to_json, status: 400 and return if weather

    location = Location.find_by(lat: location_params[:lat], lon: location_params[:lon],
                                city: location_params[:city], state: location_params[:state])
    location ||= Location.create!(location_params)
    weather_hash[:external_id] = weather_hash.delete(:id)
    weather_hash.delete(:date)
    Weather.create!(weather_hash.merge!(location_id: location.id, date_recorded: params[:date]))
    message = 'Weather successfully added'
    render json: { result: message }.to_json, status: 201
  rescue => ex
    render json: ex.message, status: 500
  end

  def list_all
    weather_array = Weather.all
    render json: build_weather_payload(weather_array), status: 200
  rescue => ex
    render json: { result: ex.message }.to_json, status: 500
  end

  def list_by_date
    weather_array = Weather.where(date_recorded: params[:date])
    message = 'Weather for given date not found'
    render json: message, status: 404 and return if weather_array.blank?
    render json: build_weather_payload(weather_array), status: 200
  rescue => ex
    render json: { result: ex.message}.to_json, status: 500
  end

  def list_by_location
    location = Location.find_by(lat: params[:latitude], lon: params[:longitude])
    message = 'Location not found'
    render json: { result: message }.to_json, status: 404 and return if location.nil?

    weather_array = Weather.where(location_id: location.id)
    message = 'Weather for given location not found'
    render json: { result: message }.to_json, status: 404 and return if weather_array.blank?

    render json: build_weather_payload(weather_array), status: 200
  rescue => ex
    render json: { result: ex.message }.to_json, status: 500
  end

  def erase
    data = Weather.joins(:location).where(lat: params[:latitude],
                                          lon: params[:longitude],
                                          date_recorded: params[:startDate]..params[:endDate])
    data.delete_all
    message = 'Weather successfully erased'
    render json: { result: message }.to_json, status: 200
  rescue => ex
    render json: { result: ex.message }.to_json, status: 500
  end

  def erase_all
    Weather.delete_all
    message = 'Weather successfully erased'
    render json: { result: message }.to_json, status: 200
  rescue => ex
    render json: { result: ex.message }.to_json, status: 500
  end

  private

  def build_location_paload(location)
    {
        lat: location.lat.to_f,
        lon: location.lon.to_f,
        city: location.city,
        state: location.state
    }
  end

  def build_weather_payload(weather_array)
    return [] if weather_array.blank?

    payload = []
    weather_array.each do |weather|
      payload << {  id: weather.id,
                    date: weather.date_recorded.strftime('%F'),
                    location: build_location_paload(weather.location),
                    temperature: weather.temperature.tr('[]', '').split(', ').map(&:to_f) }
    end
    payload.sort_by! { |weather| weather[:id] }
  end

  def weather_params
    params.permit(:id, :date, :temperature => [], :location => %i[lat lon city state])
  end
end
